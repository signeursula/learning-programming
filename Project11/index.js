let tonyaEnergy = 50;
let tonyaSatisfaction = 50;

function updateEnergy() {
    document.getElementById('tonyaEnergy').textContent = tonyaEnergy;
}

function updateSatisfaction() {
    document.getElementById('tonyaSatisfaction').textContent = tonyaSatisfaction;
}

function givedogfoodtoTonya() {
    tonyaEnergy += 12; updateEnergy(); 
    tonyaSatisfaction += 1; updateSatisfaction();
    changeTonyaImage();
}

function playwithTonya() {
    tonyaEnergy -= 9; updateEnergy(); 
    tonyaSatisfaction += 14; updateSatisfaction();
    changeTonyaImage();
}

function petTonya() {
    tonyaEnergy -= 1; updateEnergy(); 
    tonyaSatisfaction += 11; updateSatisfaction();
    changeTonyaImage();
}

function walkTonya() {
    tonyaEnergy -= 33; updateEnergy(); 
    tonyaSatisfaction -= 10; updateSatisfaction();
    changeTonyaImage();
}

function talktoTonya() {
    tonyaSatisfaction += 4; updateSatisfaction();
    changeTonyaImage();
}

updateEnergy();
updateSatisfaction();

function changeTonyaImage() {
    const tonyaImage = document.getElementById('tonyaImage');

    if (tonyaEnergy < 0) {tonyaImage.src = 'Images/poodleImage2.jpg'};

    if (tonyaEnergy > 0) {tonyaImage.src = 'Images/poodleImage1.jpg'};

    if (tonyaSatisfaction > 150) {tonyaImage.src = 'Images/poodleImage3.jpg'};

    if (tonytonyaSatisfactionaEnergy > 150) {alert("Tonya just died of joy... rip")};

    if (tonyaEnergy < -30) {alert("Oh no! Remember to feed Tonya")};
}