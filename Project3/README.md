# Catch the Light

Link to project 3: [https://signeursula.gitlab.io/learning-programming/Project3/index.html]

I must admit that I don't really like this project. During my second semester at university, I created a really good game similar to this. I wanted to recreate the success, but perhaps I didn't quite have the skills when I made this project. It is very simple and not really competitive, as it is just single-player without any form of reward. In the future, I would like to do this again, putting in more effort, and making everything look a bit nicer. One positive aspect of this project, however, is that I learned to import my own fonts and use them. It's something I have used a lot in all my subsequent projects. Well... but there is always room for improvement. All in all: not my favorite project.
