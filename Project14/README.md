# This or that

Link to project 14: [https://signeursula.gitlab.io/learning-programming/Project14/index.html]

This project draws inspiration from my virtual storage, where I found a PDF-file I created a long time ago. This file contained an extensive collection of 'this or that' questions that I had collected. I have previously enjoyed playing this game with friends, and I valued it as a means to deepen our understanding of one another. Motivated by this, I chose to breathe new life into the old project by developing a dedicated program on my GitLab. While the resulting project may not be overly complex, its creation afforded me the opportunity to enhance my skills in working with lists in programming. Moving forward, I am eager to delve into further experimentation, particularly in the realm of creating global variables. My aim is to design something more dynamic and adaptable, and steering away from too much hardcoding.
