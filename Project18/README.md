# Signe's Soundcloud Songs

Link to project 18: [https://signeursula.gitlab.io/learning-programming/Project18/index.html]

I'm at it again! Here is another project! I made this with my younger sister by my side. She is soon starting a Multimedia education program, so we practiced some website making before she actually starts her studies. While she practiced her skills, I created this new project. The project is a website about my three Soundcloud songs. Originally, I wanted there to be links and other features, but I never got to it. I still think it looks very cute! I kept the website in dark colors and used the website 'CSS Gradient' to create some cool gradients. I also downloaded some cool fonts from 'Fontspace', and I think the overall outcome is very stylish. Again, I focused on trying to make the website accessible for all formats, but it was a bit difficult. It still has some problems, but maybe I will fix them later on. At least I had fun making the project!
