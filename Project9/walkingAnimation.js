const character = document.getElementById('character');

let animationInterval;
let rightAnimationFrame = 1;
let leftAnimationFrame = 1;
let upAnimationFrame = 1;
let downAnimationFrame = 1;

//UP
//This is all I need to make the character have a walking animation to up

function animateCharacterUp() {
    upAnimationFrame = (upAnimationFrame % 4) + 1;
    if (upAnimationFrame === 1) { character.style.backgroundImage = `url('Images/walkingUpAnimation1.png')` };
    if (upAnimationFrame === 2) { character.style.backgroundImage = `url('Images/walkingUpAnimation2.png')` };
    if (upAnimationFrame === 3) { character.style.backgroundImage = `url('Images/walkingUpAnimation3.png')` };
    if (upAnimationFrame === 4) { character.style.backgroundImage = `url('Images/walkingUpAnimation4.png')` };
}

function startAnimationUp() { if (!animationInterval) { animationInterval = setInterval(animateCharacterUp, 200)}}

document.addEventListener('keydown', (event) => { if (event.key === 'w') { startAnimationUp()}});
document.addEventListener('keyup', (event) => { if (event.key === 'w') { stopAnimation()}});

function stopAnimation() {
    if (animationInterval) {
        clearInterval(animationInterval);
        animationInterval = null;
        character.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`}};



//LEFT
//This is all I need to make the character have a walking animation to the left

function animateCharacterLeft() {
    leftAnimationFrame = (leftAnimationFrame % 4) + 1;
    if (leftAnimationFrame === 1) { character.style.backgroundImage = `url('Images/walkingLeftAnimation1.png')`};
    if (leftAnimationFrame === 2) { character.style.backgroundImage = `url('Images/walkingLeftAnimation2.png')`};
    if (leftAnimationFrame === 3) { character.style.backgroundImage = `url('Images/walkingLeftAnimation3.png')`};
    if (leftAnimationFrame === 4) { character.style.backgroundImage = `url('Images/walkingLeftAnimation4.png')`};
}

function startAnimationLeft() { if (!animationInterval) { animationInterval = setInterval(animateCharacterLeft, 200) } }

document.addEventListener('keydown', (event) => { if (event.key === 'a') { startAnimationLeft()}});
document.addEventListener('keyup', (event) => { if (event.key === 'a') { stopAnimation()}});

function stopAnimation() {
    if (animationInterval) {
        clearInterval(animationInterval);
        animationInterval = null;
        character.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`}};



//DOWN
//This is all I need to make the character have a walking animation to the right

function animateCharacterDown() {
    downAnimationFrame = (downAnimationFrame % 4) + 1;
    if (downAnimationFrame === 1) { character.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`};
    if (downAnimationFrame === 2) { character.style.backgroundImage = `url('Images/walkingDownAnimation2.png')`};
    if (downAnimationFrame === 3) { character.style.backgroundImage = `url('Images/walkingDownAnimation3.png')`};
    if (downAnimationFrame === 4) { character.style.backgroundImage = `url('Images/walkingDownAnimation4.png')`};
}

function startAnimationDown() { if (!animationInterval) { animationInterval = setInterval(animateCharacterDown, 200)}}

document.addEventListener('keydown', (event) => { if (event.key === 's') { startAnimationDown()}});
document.addEventListener('keyup', (event) => { if (event.key === 's') { stopAnimation()}});

function stopAnimation() {
    if (animationInterval) {
        clearInterval(animationInterval);
        animationInterval = null;
        character.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`}};



//RIGHT
//This is all I need to make the character have a walking animation to the right

function animateCharacterRight() {
    rightAnimationFrame = (rightAnimationFrame % 4) + 1;
    if (rightAnimationFrame === 1) { character.style.backgroundImage = `url('Images/walkingRightAnimation1.png')`};
    if (rightAnimationFrame === 2) { character.style.backgroundImage = `url('Images/walkingRightAnimation2.png')`};
    if (rightAnimationFrame === 3) { character.style.backgroundImage = `url('Images/walkingRightAnimation3.png')`};
    if (rightAnimationFrame === 4) { character.style.backgroundImage = `url('Images/walkingRightAnimation4.png')`};
}

function startAnimationRight() { if (!animationInterval) { animationInterval = setInterval(animateCharacterRight, 200)}}

document.addEventListener('keydown', (event) => { if (event.key === 'd') { startAnimationRight()}});
document.addEventListener('keyup', (event) => { if (event.key === 'd') { stopAnimation()}});

function stopAnimation() {
    if (animationInterval) {
        clearInterval(animationInterval);
        animationInterval = null;
        character.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`}};
