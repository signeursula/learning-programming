var white_box_alpha = 100;

//////////////////////////////////////////////////////////////////////////////////////////

var body_image_alpha = 100;

var eyes_image_1_alpha = 0;
var eyes_image_2_alpha = 0;
var eyes_image_3_alpha = 0;

var face_image_1_alpha = 0;
var face_image_2_alpha = 0;
var face_image_3_alpha = 0;

var hair_image_1_alpha = 0;
var hair_image_2_alpha = 0;
var hair_image_3_alpha = 0;

var clothes_image_1_alpha = 0;
var clothes_image_2_alpha = 0;
var clothes_image_3_alpha = 0;

//////////////////////////////////////////////////////////////////////////////////////////////////////

let body_image = document.getElementById('body_image');

let eyes_image_1 = document.getElementById('eyes_image_1');
let eyes_image_2 = document.getElementById('eyes_image_2');
let eyes_image_3 = document.getElementById('eyes_image_3');

let face_image_1 = document.getElementById('face_image_1');
let face_image_2 = document.getElementById('face_image_2');
let face_image_3 = document.getElementById('face_image_3');

let hair_image_1 = document.getElementById('hair_image_1');
let hair_image_2 = document.getElementById('hair_image_2');
let hair_image_3 = document.getElementById('hair_image_3');

let clothes_image_1 = document.getElementById('clothes_image_1');
let clothes_image_2 = document.getElementById('clothes_image_2');
let clothes_image_3 = document.getElementById('clothes_image_3');

//////////////////////////////////////////////////////////////////////////////////////////////////////

document.documentElement.style.setProperty('--body_image_alpha', body_image_alpha);

document.documentElement.style.setProperty('--eyes_image_1_alpha', eyes_image_1_alpha);
document.documentElement.style.setProperty('--eyes_image_2_alpha', eyes_image_2_alpha);
document.documentElement.style.setProperty('--eyes_image_3_alpha', eyes_image_3_alpha);

document.documentElement.style.setProperty('--face_image_1_alpha', face_image_1_alpha);
document.documentElement.style.setProperty('--face_image_2_alpha', face_image_2_alpha);
document.documentElement.style.setProperty('--face_image_3_alpha', face_image_3_alpha);

document.documentElement.style.setProperty('--hair_image_1_alpha', hair_image_1_alpha);
document.documentElement.style.setProperty('--hair_image_2_alpha', hair_image_2_alpha);
document.documentElement.style.setProperty('--hair_image_3_alpha', hair_image_3_alpha);

document.documentElement.style.setProperty('--clothes_image_1_alpha', clothes_image_1_alpha);
document.documentElement.style.setProperty('--clothes_image_2_alpha', clothes_image_2_alpha);
document.documentElement.style.setProperty('--clothes_image_3_alpha', clothes_image_3_alpha);

//////////////////////////////////////////////////////////////////////////////////////////////////////

let question11 = document.getElementById('question11');
let question12 = document.getElementById('question12');
let question13 = document.getElementById('question13');

let question21 = document.getElementById('question21');
let question22 = document.getElementById('question22');
let question23 = document.getElementById('question23');

let question31 = document.getElementById('question31');
let question32 = document.getElementById('question32');
let question33 = document.getElementById('question33');

let question41 = document.getElementById('question41');
let question42 = document.getElementById('question42');
let question43 = document.getElementById('question43');

//////////////////////////////////////////////////////////////////////////////////////////////////////

let white_box = document.getElementById('white_box');

//////////////////////////////////////////////////////////////////////////////////////////////////////

function updateStyles() {
    document.documentElement.style.setProperty('--eyes_image_1_alpha', eyes_image_1_alpha / 100);
    document.documentElement.style.setProperty('--eyes_image_2_alpha', eyes_image_2_alpha / 100);
    document.documentElement.style.setProperty('--eyes_image_3_alpha', eyes_image_3_alpha / 100);

    document.documentElement.style.setProperty('--face_image_1_alpha', face_image_1_alpha / 100);
    document.documentElement.style.setProperty('--face_image_2_alpha', face_image_2_alpha / 100);
    document.documentElement.style.setProperty('--face_image_3_alpha', face_image_3_alpha / 100);

    document.documentElement.style.setProperty('--hair_image_1_alpha', hair_image_1_alpha / 100);
    document.documentElement.style.setProperty('--hair_image_2_alpha', hair_image_2_alpha / 100);
    document.documentElement.style.setProperty('--hair_image_3_alpha', hair_image_3_alpha / 100);

    document.documentElement.style.setProperty('--clothes_image_1_alpha', clothes_image_1_alpha / 100);
    document.documentElement.style.setProperty('--clothes_image_2_alpha', clothes_image_2_alpha / 100);
    document.documentElement.style.setProperty('--clothes_image_3_alpha', clothes_image_3_alpha / 100);

    document.documentElement.style.setProperty('--white_box_alpha', white_box_alpha / 100);
    requestAnimationFrame(updateStyles);
}

requestAnimationFrame(updateStyles);

//////////////////////////////////////////////////////////////////////////////////////////////////////

question11.addEventListener('click', function () {
    eyes_image_1_alpha = 100;
    eyes_image_2_alpha = 0;
    eyes_image_3_alpha = 0;

    question11.style.backgroundColor = 'white';
    question11.style.color = 'black';
    question12.style.backgroundColor = 'black';
    question12.style.color = 'white';
    question13.style.backgroundColor = 'black';
    question13.style.color = 'white';
});

question12.addEventListener('click', function () {
    eyes_image_1_alpha = 0;
    eyes_image_2_alpha = 100;
    eyes_image_3_alpha = 0;

    question11.style.backgroundColor = 'black';
    question11.style.color = 'white';
    question12.style.backgroundColor = 'white';
    question12.style.color = 'black';
    question13.style.backgroundColor = 'black';
    question13.style.color = 'white';
});

question13.addEventListener('click', function () {
    eyes_image_1_alpha = 0;
    eyes_image_2_alpha = 0;
    eyes_image_3_alpha = 100;

    question11.style.backgroundColor = 'black';
    question11.style.color = 'white';
    question12.style.backgroundColor = 'black';
    question12.style.color = 'white';
    question13.style.backgroundColor = 'white';
    question13.style.color = 'black';
});

//////////////////////////////////////////////////////////////////////////////////////////////////////

question21.addEventListener('click', function () {
    face_image_1_alpha = 100;
    face_image_2_alpha = 0;
    face_image_3_alpha = 0;

    question21.style.backgroundColor = 'white';
    question21.style.color = 'black';
    question22.style.backgroundColor = 'black';
    question22.style.color = 'white';
    question23.style.backgroundColor = 'black';
    question23.style.color = 'white';
});

question22.addEventListener('click', function () {
    face_image_1_alpha = 0;
    face_image_2_alpha = 100;
    face_image_3_alpha = 0;

    question21.style.backgroundColor = 'black';
    question21.style.color = 'white';
    question22.style.backgroundColor = 'white';
    question22.style.color = 'black';
    question23.style.backgroundColor = 'black';
    question23.style.color = 'white';
});

question23.addEventListener('click', function () {
    face_image_1_alpha = 0;
    face_image_2_alpha = 0;
    face_image_3_alpha = 100;

    question21.style.backgroundColor = 'black';
    question21.style.color = 'white';
    question22.style.backgroundColor = 'black';
    question22.style.color = 'white';
    question23.style.backgroundColor = 'white';
    question23.style.color = 'black';
});

//////////////////////////////////////////////////////////////////////////////////////////

question31.addEventListener('click', function () {
    hair_image_1_alpha = 100;
    hair_image_2_alpha = 0;
    hair_image_3_alpha = 0;

    question31.style.backgroundColor = 'white';
    question31.style.color = 'black';
    question32.style.backgroundColor = 'black';
    question32.style.color = 'white';
    question33.style.backgroundColor = 'black';
    question33.style.color = 'white';
});

question32.addEventListener('click', function () {
    hair_image_1_alpha = 0;
    hair_image_2_alpha = 100;
    hair_image_3_alpha = 0;

    question31.style.backgroundColor = 'black';
    question31.style.color = 'white';
    question32.style.backgroundColor = 'white';
    question32.style.color = 'black';
    question33.style.backgroundColor = 'black';
    question33.style.color = 'white';
});

question33.addEventListener('click', function () {
    hair_image_1_alpha = 0;
    hair_image_2_alpha = 0;
    hair_image_3_alpha = 100;

    question31.style.backgroundColor = 'black';
    question31.style.color = 'white';
    question32.style.backgroundColor = 'black';
    question32.style.color = 'white';
    question33.style.backgroundColor = 'white';
    question33.style.color = 'black';
});

//////////////////////////////////////////////////////////////////////////////////////////

question41.addEventListener('click', function () {
    clothes_image_1_alpha = 100;
    clothes_image_2_alpha = 0;
    clothes_image_3_alpha = 0;

    question41.style.backgroundColor = 'white';
    question41.style.color = 'black';
    question42.style.backgroundColor = 'black';
    question42.style.color = 'white';
    question43.style.backgroundColor = 'black';
    question43.style.color = 'white';
});

question42.addEventListener('click', function () {
    clothes_image_1_alpha = 0;
    clothes_image_2_alpha = 100;
    clothes_image_3_alpha = 0;

    question41.style.backgroundColor = 'black';
    question41.style.color = 'white';
    question42.style.backgroundColor = 'white';
    question42.style.color = 'black';
    question43.style.backgroundColor = 'black';
    question43.style.color = 'white';
});

question43.addEventListener('click', function () {
    clothes_image_1_alpha = 0;
    clothes_image_2_alpha = 0;
    clothes_image_3_alpha = 100;

    question41.style.backgroundColor = 'black';
    question41.style.color = 'white';
    question42.style.backgroundColor = 'black';
    question42.style.color = 'white';
    question43.style.backgroundColor = 'white';
    question43.style.color = 'black';
});

//////////////////////////////////////////////////////////////////////////////////////////

reveal_button.addEventListener('click', function () {
    white_box_alpha = 0;
});