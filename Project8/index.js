console.log("Hello World!")

const player = document.getElementById('player');
const canvas = document.getElementById("canvas");
const map = document.getElementById("map");
const foreground = document.getElementById("foreground");

let animationInterval;
let rightAnimationFrame = 1;
let leftAnimationFrame = 1;
let upAnimationFrame = 1;
let downAnimationFrame = 1;

let playerStartingX = (canvas.clientWidth / 2);
let playerStartingY = (canvas.clientHeight / 2);
let mapStartingX = -1340;
let mapStartingY = -800;
let playerMovement = 10;

//UP
//This is all I need to make the player have a walking animation to up

function animatePlayerUp() {
    upAnimationFrame = (upAnimationFrame % 4) + 1;
    if (upAnimationFrame === 1) { player.style.backgroundImage = `url('Images/walkingUpAnimation1.png')` };
    if (upAnimationFrame === 2) { player.style.backgroundImage = `url('Images/walkingUpAnimation2.png')` };
    if (upAnimationFrame === 3) { player.style.backgroundImage = `url('Images/walkingUpAnimation3.png')` };
    if (upAnimationFrame === 4) { player.style.backgroundImage = `url('Images/walkingUpAnimation4.png')` };
}

function startAnimationUp() { if (!animationInterval) { animationInterval = setInterval(animatePlayerUp, 200)}}

document.addEventListener('keydown', (event) => { if (event.key === 'w') { startAnimationUp()}});
document.addEventListener('keyup', (event) => { if (event.key === 'w') { stopAnimation()}});

function stopAnimation() {
    if (animationInterval) {
        clearInterval(animationInterval);
        animationInterval = null;
        player.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`}};

//LEFT
//This is all I need to make the player have a walking animation to the left

function animatePlayerLeft() {
    leftAnimationFrame = (leftAnimationFrame % 4) + 1;
    if (leftAnimationFrame === 1) { player.style.backgroundImage = `url('Images/walkingLeftAnimation1.png')`};
    if (leftAnimationFrame === 2) { player.style.backgroundImage = `url('Images/walkingLeftAnimation2.png')`};
    if (leftAnimationFrame === 3) { player.style.backgroundImage = `url('Images/walkingLeftAnimation3.png')`};
    if (leftAnimationFrame === 4) { player.style.backgroundImage = `url('Images/walkingLeftAnimation4.png')`};
}

function startAnimationLeft() { if (!animationInterval) { animationInterval = setInterval(animatePlayerLeft, 200) } }

document.addEventListener('keydown', (event) => { if (event.key === 'a') { startAnimationLeft()}});
document.addEventListener('keyup', (event) => { if (event.key === 'a') { stopAnimation()}});

function stopAnimation() {
    if (animationInterval) {
        clearInterval(animationInterval);
        animationInterval = null;
        player.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`}};

//DOWN
//This is all I need to make the player have a walking animation to the right

function animatePlayerDown() {
    downAnimationFrame = (downAnimationFrame % 4) + 1;
    if (downAnimationFrame === 1) { player.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`};
    if (downAnimationFrame === 2) { player.style.backgroundImage = `url('Images/walkingDownAnimation2.png')`};
    if (downAnimationFrame === 3) { player.style.backgroundImage = `url('Images/walkingDownAnimation3.png')`};
    if (downAnimationFrame === 4) { player.style.backgroundImage = `url('Images/walkingDownAnimation4.png')`};
}

function startAnimationDown() { if (!animationInterval) { animationInterval = setInterval(animatePlayerDown, 200)}}

document.addEventListener('keydown', (event) => { if (event.key === 's') { startAnimationDown()}});
document.addEventListener('keyup', (event) => { if (event.key === 's') { stopAnimation()}});

function stopAnimation() {
    if (animationInterval) {
        clearInterval(animationInterval);
        animationInterval = null;
        player.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`}};

//RIGHT
//This is all I need to make the player have a walking animation to the right

function animatePlayerRight() {
    rightAnimationFrame = (rightAnimationFrame % 4) + 1;
    if (rightAnimationFrame === 1) { player.style.backgroundImage = `url('Images/walkingRightAnimation1.png')`};
    if (rightAnimationFrame === 2) { player.style.backgroundImage = `url('Images/walkingRightAnimation2.png')`};
    if (rightAnimationFrame === 3) { player.style.backgroundImage = `url('Images/walkingRightAnimation3.png')`};
    if (rightAnimationFrame === 4) { player.style.backgroundImage = `url('Images/walkingRightAnimation4.png')`};
}

function startAnimationRight() { if (!animationInterval) { animationInterval = setInterval(animatePlayerRight, 200)}}

document.addEventListener('keydown', (event) => { if (event.key === 'd') { startAnimationRight()}});
document.addEventListener('keyup', (event) => { if (event.key === 'd') { stopAnimation()}});

function stopAnimation() {
    if (animationInterval) {
        clearInterval(animationInterval);
        animationInterval = null;
        player.style.backgroundImage = `url('Images/walkingDownAnimation1.png')`}};



//REST OF THE CODE

player.style.left = playerStartingX + "px";
player.style.top = playerStartingY + "px";

function reload() {
    document.addEventListener('keydown', function(event) {
        if (event.key === 'w') {
            mapStartingY += playerMovement;
            if (mapStartingY > -230) {mapStartingY = -230};
            map.style.backgroundPositionY = mapStartingY + "px";
            foreground.style.backgroundPositionY = mapStartingY + "px";}
            })
    
    document.addEventListener('keydown', function(event) {
        if (event.key === 'a') {
            mapStartingX += playerMovement;
            if (mapStartingX > -290) {mapStartingX = -290};
            map.style.backgroundPositionX = mapStartingX + "px";
            foreground.style.backgroundPositionX = mapStartingX + "px";}
            })
    
    document.addEventListener('keydown', function(event) {
        if (event.key === 's') {
            mapStartingY -= playerMovement;
            if (mapStartingY < -1220) {mapStartingY = -1220};
            map.style.backgroundPositionY = mapStartingY + "px";
            foreground.style.backgroundPositionY = mapStartingY + "px";}
            }) 
    
    document.addEventListener('keydown', function(event) {
        if (event.key === 'd') {
            mapStartingX -= playerMovement;
            if (mapStartingX < -2390) {mapStartingX = -2390};
            map.style.backgroundPositionX = mapStartingX + "px";
            foreground.style.backgroundPositionX = mapStartingX + "px";}
            })

}
reload();
