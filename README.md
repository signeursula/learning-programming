# ✿ Learning Programming ✿

Hello World! My name is Signe, and this is my GitLab where I will practice my programming skills. I will practice coding using ChatGPT and YouTube videos, but I am not following any specific course. Programming is a new hobby of mine, and I first started programming in February 2023. I was introduced to programming through my studies because I am studying Digital Design. This has opened a whole new world for me, and I see programming as a significant opportunity to be creative and to create something. I also find it highly relevant since everything is connected to the internet today. I have no specific theme for what to code, so I am probably just going to make a lot of different mini-projects. For example, useless websites, mini-games, or digital art. I really enjoy making games, especially pixel games, so maybe one day I will create a little arcade here on GitLab. We'll see. For now, I will code whatever comes to my mind! My plan for now is to start learning basic HTML, CSS, and JavaScript. As I progress, I dream of also learning Python and C#. Wish me luck! Someday I would also like to create a launcher for my coding projects, but right now, all of my projects will be linked in this readme. Thank you for reading along.

Project 1: [https://signeursula.gitlab.io/learning-programming/Project1/index.html]

Project 2: [https://signeursula.gitlab.io/learning-programming/Project2/index.html]

Project 3: [https://signeursula.gitlab.io/learning-programming/Project3/index.html]

Project 4: [https://signeursula.gitlab.io/learning-programming/Project4/Home/index.html]

Project 5: [https://signeursula.gitlab.io/learning-programming/Project5/Page1/index.html]

Project 6: [https://signeursula.gitlab.io/learning-programming/Project6/index.html]

Project 7: [https://signeursula.gitlab.io/learning-programming/Project7/index.html]

Project 8: [https://signeursula.gitlab.io/learning-programming/Project8/index.html]

Project 9: [https://signeursula.gitlab.io/learning-programming/Project9/index.html]

Project 10: [https://signeursula.gitlab.io/learning-programming/Project10/index.html]

Project 11: [https://signeursula.gitlab.io/learning-programming/Project11/index.html]

Project 12: [https://signeursula.gitlab.io/learning-programming/Project12/index.html]

Project 13: [https://signeursula.gitlab.io/learning-programming/Project13/Forside/index.html]

Project 14: [https://signeursula.gitlab.io/learning-programming/Project14/index.html]

Project 15: [https://signeursula.gitlab.io/learning-programming/Project15/index.html]

Project 16: [https://signeursula.gitlab.io/learning-programming/Project16/index.html]

Project 17: [https://signeursula.gitlab.io/learning-programming/Project17/index.html]

Project 18: [https://signeursula.gitlab.io/learning-programming/Project18/index.html]

Project 19: [https://signeursula.gitlab.io/learning-programming/Project19/Forside/index.html]

Project 20: [https://signeursula.gitlab.io/learning-programming/Project20/index.html]