# My favorite songs

Link to project 2: [https://signeursula.gitlab.io/learning-programming/Project2/index.html]

This is my second coding project. In this project, I showcase my favorite songs. Since I am a huge optimist, I have a bit of a bad habit of saying that all songs are my favorite songs, so this website displays my specific favorite songs! If you ask me in five years, I will probably have some completely different favorite songs, who knows? But in any case, this is a website that shows my current favorite songs. The website itself has a gradient background, which is actually just an image. Later on, I learned how to create the background color with a gradient, but here I just used an image. Throughout my coding process for this project, I also became much better at CSS, which is a skill that I have really come to appreciate. The website is very static, but nevertheless, I am very proud of it.
