var red_color = document.getElementById("red_color");
var orange_color = document.getElementById("orange_color");
var green_color = document.getElementById("green_color");
var blue_color = document.getElementById("blue_color");
var pink_color = document.getElementById("pink_color");

let chosen_colorcode = "#FFFFFF";

let white_colorcode = "#FFFFFF";
let red_colorcode = "#f74e30";
let orange_colorcode = "#f79f30";
let green_colorcode = "#78e43a";
let blue_colorcode = "#469cf4";
let pink_colorcode = "#d878db";

////////////////////////////////////////////////////////////////////////////////////////////////

red_color.addEventListener("click", function () {
    if (red_color.style.opacity === "1") { red_color.style.opacity = "0"; }
    else {
        red_color.style.opacity = "1";
        orange_color.style.opacity = "0";
        green_color.style.opacity = "0";
        blue_color.style.opacity = "0";
        pink_color.style.opacity = "0";
    }
});

orange_color.addEventListener("click", function () {
    if (orange_color.style.opacity === "1") { orange_color.style.opacity = "0"; }
    else {

        red_color.style.opacity = "0";
        orange_color.style.opacity = "1";
        green_color.style.opacity = "0";
        blue_color.style.opacity = "0";
        pink_color.style.opacity = "0";
    }
});

green_color.addEventListener("click", function () {
    if (green_color.style.opacity === "1") { green_color.style.opacity = "0"; }
    else {

        red_color.style.opacity = "0";
        orange_color.style.opacity = "0";
        green_color.style.opacity = "1";
        blue_color.style.opacity = "0";
        pink_color.style.opacity = "0";
    }
});

blue_color.addEventListener("click", function () {
    if (blue_color.style.opacity === "1") { blue_color.style.opacity = "0"; }
    else {

        red_color.style.opacity = "0";
        orange_color.style.opacity = "0";
        green_color.style.opacity = "0";
        blue_color.style.opacity = "1";
        pink_color.style.opacity = "0";
    }
});

pink_color.addEventListener("click", function () {
    if (pink_color.style.opacity === "1") { pink_color.style.opacity = "0"; }
    else {

        red_color.style.opacity = "0";
        orange_color.style.opacity = "0";
        green_color.style.opacity = "0";
        blue_color.style.opacity = "0";
        pink_color.style.opacity = "1";
    }
});

////////////////////////////////////////////////////////////////////////////////////////////////

red_color.addEventListener("click", function () {
    if (chosen_colorcode === white_colorcode || 
        chosen_colorcode === orange_colorcode || 
        chosen_colorcode === green_colorcode || 
        chosen_colorcode === blue_colorcode || 
        chosen_colorcode === pink_colorcode) 
    
    {chosen_colorcode = red_colorcode}
    else {chosen_colorcode = white_colorcode;
    }
});

orange_color.addEventListener("click", function () {
    if (chosen_colorcode === white_colorcode || 
        chosen_colorcode === red_colorcode || 
        chosen_colorcode === green_colorcode || 
        chosen_colorcode === blue_colorcode || 
        chosen_colorcode === pink_colorcode) 
    
    {chosen_colorcode = orange_colorcode}
    else {chosen_colorcode = white_colorcode;
    }
});

green_color.addEventListener("click", function () {
    if (chosen_colorcode === white_colorcode || 
        chosen_colorcode === red_colorcode || 
        chosen_colorcode === orange_colorcode || 
        chosen_colorcode === blue_colorcode || 
        chosen_colorcode === pink_colorcode) 
    
    {chosen_colorcode = green_colorcode}
    else {chosen_colorcode = white_colorcode;
    }
});

blue_color.addEventListener("click", function () {
    if (chosen_colorcode === white_colorcode || 
        chosen_colorcode === red_colorcode || 
        chosen_colorcode === green_colorcode || 
        chosen_colorcode === orange_colorcode || 
        chosen_colorcode === pink_colorcode) 
    
    {chosen_colorcode = blue_colorcode}
    else {chosen_colorcode = white_colorcode;
    }
});

pink_color.addEventListener("click", function () {
    if (chosen_colorcode === white_colorcode || 
        chosen_colorcode === red_colorcode || 
        chosen_colorcode === green_colorcode || 
        chosen_colorcode === blue_colorcode || 
        chosen_colorcode === orange_colorcode) 
    
    {chosen_colorcode = pink_colorcode}
    else {chosen_colorcode = white_colorcode;
    }
});

////////////////////////////////////////////////////////////////////////////////////////////////

function consoleLogColor() {
    console.log(chosen_colorcode);
}

setInterval(consoleLogColor, 1000);

////////////////////////////////////////////////////////////////////////////////////////////////


