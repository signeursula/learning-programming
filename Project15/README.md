# Changing for the better and sometimes for the worse

Link to project 15: [https://signeursula.gitlab.io/learning-programming/Project15/index.html]

This project is all about change. It started with the idea of customizing your own website. The visions I had were maybe a bit too ambitious, so instead, I wanted the user to be able to choose the color of the website's background. I made four buttons at the top left corner, and then if you click on a color, the whole background of the website changes to the chosen color. I found this very cool myself! I like making more interactive websites. It is fun that you, as a user, can do something. Along with the theme of changing the background, I also made a weird little text about changing (with the help from ChatGPT). I also inserted some pictures I found on Pinterest. All in all, it is an okay project. Not that complex, but it was fun to make. More projects are to come.
