console.log("Hello World!")

const canvas = document.getElementById("canvas");
const map = document.getElementById("map");
const character = document.getElementById("character");
const collision1 = document.getElementById("collision1");

let characterStartingX = (character.style.left);
let characterStartingY = (character.style.left);
let mapStartingX = -1800;
let mapStartingY = -400;
let characterMovement = 20;

character.style.left = characterStartingX + "px";
character.style.top = characterStartingY + "px";

function reload() {
    document.addEventListener('keydown', function(event) {
        if (event.key === 'w') {
            mapStartingY += characterMovement;
            map.style.backgroundPositionY = mapStartingY + "px";}
            })
    
    document.addEventListener('keydown', function(event) {
        if (event.key === 'a') {
            mapStartingX += characterMovement;
            map.style.backgroundPositionX = mapStartingX + "px";}
            })
    
    document.addEventListener('keydown', function(event) {
        if (event.key === 's') {
            mapStartingY -= characterMovement;
            map.style.backgroundPositionY = mapStartingY + "px";}
            }) 
    
    document.addEventListener('keydown', function(event) {
        if (event.key === 'd') {
            mapStartingX -= characterMovement;
            map.style.backgroundPositionX = mapStartingX + "px";}

            console.log("top:" + map.style.backgroundPositionY);
            console.log("left:" + map.style.backgroundPositionX);
            })

    document.addEventListener('keydown', function(event) {
        if (mapStartingY > -300) {mapStartingY = -300};
        if (mapStartingY < -1160) {mapStartingY = -1160};
        if (mapStartingX < -2340) {mapStartingX = -2340};
        if (mapStartingX > -20) {mapStartingX = -20};

        })
}
reload();
